#include "TCPClient.h"
#include <iostream>
#include <sstream>

#pragma comment(lib, "ws2_32.lib")

TCPClient::TCPClient(void)
{
    sockd = NULL;

    if (WSAStartup(0x202, &wsa))
    {
        std::stringstream ss;
        ss << "WSAStart error " << WSAGetLastError();
        
    }
   
    sockd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockd == SOCKET_ERROR)
    {
        std::stringstream ss;
        ss << "Socket() error " << WSAGetLastError();
        throw std::exception(ss.str().c_str());
    }
}


TCPClient::~TCPClient(void)
{
    Disconnect();
    WSACleanup();
}
sockaddr_in TCPClient::GetAddress(std::string const & addr, unsigned short port)
{
    sockaddr_in dest_addr;
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(port);
    HOSTENT *hst;

    if (inet_addr(addr.c_str()) != INADDR_NONE)
    {
        dest_addr.sin_addr.s_addr = inet_addr(addr.c_str());
    }
    else 
    {
        if (hst = gethostbyname(addr.c_str()))
        ((unsigned long *)&dest_addr.sin_addr)[0]=
        ((unsigned long **)hst->h_addr_list)[0][0];
        else 
        {
            throw std::exception("Can't resolve address");
        }
    }
    return dest_addr;
}
bool TCPClient::Connect(std::string const &addr, unsigned short port)
{
    sockaddr_in dst_addr;
    try
    {
        dst_addr = GetAddress(addr, port);
    }catch(std::exception &e)
    {
        Log(e.what());
        return false;
    }
    const sockaddr * dst_paddr = reinterpret_cast<const sockaddr *>(&dst_addr);
    if (connect(sockd, dst_paddr, sizeof(dst_addr)) != ERROR_SUCCESS)
    {
        std::stringstream ss;
        ss << "Connect error " << WSAGetLastError();
        Log(ss.str());
        return false;
    }

    return true;
}

void TCPClient::Disconnect()
{
    if(sockd)
    {
        if(closesocket(sockd) == ERROR_SUCCESS)
            sockd = NULL;
    }
}

size_t TCPClient::Send(std::string const &data)
{
    return send(sockd, data.c_str(), data.size(), NULL);
}
std::string TCPClient::Receive()
{
    return Receive(SizeOfBuffer);
}
std::string TCPClient::Receive(size_t count)
{
    std::string ret;
    char * buff = new char[count];

    int nsize;
    nsize = recv(sockd, buff, count, NULL);
    if(nsize == SOCKET_ERROR)
        return ret;

    ret.assign(buff, &buff[nsize]);
    delete [] buff;
    return ret;
}